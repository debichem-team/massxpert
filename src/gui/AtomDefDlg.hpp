/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef ATOM_DEF_DLG_HPP
#define ATOM_DEF_DLG_HPP


/////////////////////// Local includes
#include "../nongui/Atom.hpp"
#include "AbstractPolChemDefDependentDlg.hpp"
#include "ui_AtomDefDlg.h"


namespace msxps
{

	namespace massxpert
	{



class PolChemDef;
class PolChemDefWnd;
class Atom;
class Isotope;


class AtomDefDlg : public AbstractPolChemDefDependentDlg
{
  Q_OBJECT

  private:
  Ui::AtomDefDlg m_ui;

  QList<Atom *> *mp_list;

  void closeEvent(QCloseEvent *event);

  void writeSettings();
  void readSettings();


  public:
  AtomDefDlg(PolChemDef *polChemDef,
             PolChemDefWnd *polChemDefWnd,
             const QString &settingsFilePath);

  ~AtomDefDlg();

  bool initialize();

  void updateAtomIdentityDetails(Atom *);
  void updateAtomMassDetails(Atom *);
  void updateIsotopeDetails(Isotope *);

  void clearAllDetails();

  public slots:

  // ACTIONS
  void addAtomPushButtonClicked();
  void removeAtomPushButtonClicked();
  void moveUpAtomPushButtonClicked();
  void moveDownAtomPushButtonClicked();

  void addIsotopePushButtonClicked();
  void removeIsotopePushButtonClicked();
  void moveUpIsotopePushButtonClicked();
  void moveDownIsotopePushButtonClicked();

  void applyAtomPushButtonClicked();
  void applyIsotopePushButtonClicked();

  bool validatePushButtonClicked();

  // VALIDATION
  bool validate();

  // WIDGETRY
  void atomListWidgetItemSelectionChanged();
  void isotopeListWidgetItemSelectionChanged();
};

} // namespace massxpert

} // namespace msxps


#endif // ATOM_DEF_DLG_HPP
