/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MODIF_SPEC_HPP
#define MODIF_SPEC_HPP


/////////////////////// Qt includes
#include <QString>


namespace msxps {


	namespace massxpert
	{


enum
{
  COMPOSITING_OPAQUE      = 1 << 0,
  COMPOSITING_TRANSPARENT = 1 << 1,
};


class ModifSpec
{
  private:
  QString m_name;
  int m_action;
  QString m_raster;
  QString m_vector;
  QString m_sound;

  public:
  ModifSpec();

  void setName(const QString &);
  const QString &name();

  void setAction(int);
  int action();

  void setRaster(const QString &);
  const QString &raster();

  void setVector(const QString &);
  const QString &vector();

  void setSound(const QString &);
  const QString &sound();

  static bool parseFile(QString &, QList<ModifSpec *> *);
};

} // namespace massxpert

} // namespace msxps


#endif // MODIF_SPEC_HPP
