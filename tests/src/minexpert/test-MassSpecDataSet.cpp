
#include <iostream>
#include <ctime>
#include <random>

#include <catch.hpp>

#include <MassSpecDataSet.hpp>
#include <MassSpecDataFileFormatAnalyzer.hpp>
#include <MassSpecDataFileLoaderSqlite3.hpp>
#include <MassSpecDataFileLoaderPwiz.hpp>
#include <MassSpecDataFileLoaderPwiz.hpp>
#include <MassSpecDataFileLoaderMs1.hpp>
#include <MassSpecDataFileLoaderXy.hpp>
#include <MassSpecDataFileLoaderBrukerXy.hpp>
#include <MassSpecDataFileLoaderDx.hpp>
#include <globals/globals.hpp>

#include <QDebug>
#include <QFile>

using std::cout;

#define TEST_DATA_DIR "/home/rusconi/devel/msxpertsuite/tests/data"

namespace msXpSmineXpert
{

TEST_CASE("Construct a mass spec data set.", "[MassSpecDataSet]")
{
  MassSpecDataSet massSpecDataSet0;

  QObject parentObject;

  QString name     = "The name";
  QString fileName = "the-file-name.ext";

  MassSpecDataSet massSpecDataSet1(&parentObject, name, fileName, true);

  REQUIRE(massSpecDataSet1.name() == name);
  REQUIRE(massSpecDataSet1.fileName() == fileName);
  REQUIRE(massSpecDataSet1.isStreamed() == true);
}

TEST_CASE("Construct a mass spec data set via loading a file.",
          "[MassSpecDataSet]")
{
  QString fileName = QString("%1/mzml/protein-mobility-waters-synapt-hdms.mzml")
                       .arg(TEST_DATA_DIR);

  int format = MassSpecDataFileFormatAnalyzer::canLoadData(fileName);
  REQUIRE(format == MassSpecDataFileFormat::MASS_SPEC_DATA_FILE_FORMAT_MZML);

  MassSpecDataFileLoaderPwiz loader(fileName);

  // This data set will be reparented when we provide it to the handling
  // widget (window or plot widget or whatever QObject).
  MassSpecDataSet *massSpecDataSet =
    new MassSpecDataSet(Q_NULLPTR, "NOT_SET", fileName, /* isStreamed */ false);

  int res = loader.loadDataToMassSpecDataSet(massSpecDataSet);

  REQUIRE(res == 4970);

  // At this point I stop here because I think a redesign is required such
  // that all the integrations be not handled by GUI elements, like the
  // TicChromWnd who is in charge of computing the initial TIC or the
  // MassSpecWnd who is in charge of computing mass spectra. That is not
  // efficient, these integrations should be performed by non-GUI objects like
  // ToMassSpecIntegrator or ToTicChromIntegrator or ToColorMapIntegrator or
  // ToDriftSpectrumIntegrator....
}


} // namespace msXpSmineXpert
