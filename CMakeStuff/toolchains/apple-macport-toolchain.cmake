message("APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES "/usr/include" "/opt/local/include")
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES "/usr/include" "/opt/local/include")
link_directories("/opt/local/lib")

# This is used throughout all the build system files
set(TARGET massXpert)

# Now that we know what is the TARGET (in the toolchain files above,
# we can compute the lowercase TARGET (used for string replacements in 
# configure files and also for the resource compilation with windres.exe.
string(TOLOWER ${TARGET} TARGET_LOWERCASE)
message("TARGET_LOWERCASE: ${TARGET_LOWERCASE}")

set(HOME_DEVEL_DIR "/Users/rusconi/devel")


add_definitions("-DAPPLE=1")
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mmacosx-version-min=10.13"
  CACHE STRING "Flags used by the compiler during all build types." FORCE)

#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fvisibility-inlines-hidden"
#CACHE STRING "Flags used by the compiler during all build types." FORCE)

set(CMAKE_SYSROOT
  "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk"
  CACHE STRING "MacOSX10.14.sdk." FORCE)


#add_definitions("-stdlib=libc++")
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
#message(STATUS "using ${CMAKE_CXX_FLAGS}")


## INSTALL directories
set(BUNDLE_DIR ${CMAKE_CURRENT_BINARY_DIR}/${TARGET}.app)
set(CMAKE_INSTALL_PREFIX "${BUNDLE_DIR}/Contents")
set(BIN_DIR ${BUNDLE_DIR}/Contents/MacOS)
set(DATA_DIR ${BUNDLE_DIR}/Contents/Resources/data)
set(DOC_DIR ${BUNDLE_DIR}/Contents/doc)


add_definitions(-fPIC)

