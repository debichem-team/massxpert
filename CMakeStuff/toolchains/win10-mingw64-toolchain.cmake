message("WIN10-MINGW64 environment https://www.msys2.org/")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES "c:/msys64/mingw64/include")
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES "c:/msys64/mingw64/include")

# This is used throughout all the build system files
set(TARGET massXpert)

# Now that we know what is the TARGET (in the toolchain files above,
# we can compute the lowercase TARGET (used for string replacements in 
# configure files and also for the resource compilation with windres.exe.
string(TOLOWER ${TARGET} TARGET_LOWERCASE)
message("TARGET_LOWERCASE: ${TARGET_LOWERCASE}")

set(HOME_DEVEL_DIR "$ENV{HOME}/devel")

CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/CMakeStuff/massxpert-mingw64-win7+.iss.in
  ${CMAKE_SOURCE_DIR}/winInstaller/${TARGET_LOWERCASE}-mingw64-win7+.iss @ONLY)

## INSTALL directories
# This is the default on windows, but set it nonetheless.
set(CMAKE_INSTALL_PREFIX "C:/Program Files")
set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/${TARGET})
set(DATA_DIR ${CMAKE_INSTALL_PREFIX}/${TARGET}/data)
# On Win, the doc dir is uppercase.
set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/${TARGET}/doc)

## Add the platform-specific source (that is the icon resource)
#  after we have built it using windres.exe.
set(PLATFORM_SPECIFIC_SOURCES ${CMAKE_SOURCE_DIR}/${TARGET}.rc)

if(NOT CMAKE_RC_COMPILER)
  set(CMAKE_RC_COMPILER windres.exe)
endif()

execute_process(COMMAND ${CMAKE_RC_COMPILER} 
  -D GCC_WINDRES
  -I "${CMAKE_CURRENT_SOURCE_DIR}"
  -i "${CMAKE_SOURCE_DIR}/${TARGET_LOWERCASE}.rc"
  -o "${CMAKE_CURRENT_BINARY_DIR}/${TARGET}.obj"
  WORKING_DIRECTORY	${CMAKE_CURRENT_SOURCE_DIR})

set(PLATFORM_SPECIFIC_SOURCES "${CMAKE_CURRENT_BINARY_DIR}/${TARGET}.obj")


# On Win10 all the code is relocatable.
remove_definitions(-fPIC)


